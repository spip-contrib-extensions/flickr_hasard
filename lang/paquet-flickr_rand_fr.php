<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/crayons/lang/
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'flickr_rand_description' => 'Affiche une image aléatoire (ou non) venant de Flickr avec ses indications de titre, auteur et licence
    
      Ce plugin utilise l\'API Flickr mais n\'est ni soutenu, ni certifié par Flickr.',
	'flickr_rand_nom' => 'Flickr hasard',
	'flickr_rand_slogan' => 'Affiche aléatoirement une image Flickr'
);

