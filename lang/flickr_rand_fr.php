<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://www.spip.net/trad-lang/
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'explication_plage' => '  Indiquer un chiffre entre 1 et 4000 (valeur conseillée: 200, maximum autorisé par Flickr: 4000). 
                            Plus le chiffre est grand, plus vos images seront variées mais moins elles seront récentes',
	'explication_blacklist' => 'Auteurs dont les images sont à exclure (séparer leurs surnoms flickr par un ";")',

	// L
	'label_plage' => 'Plage de recherche de vos images aléatoires',
	'label_blacklist' => 'Liste noire des auteurs',

	//  T
	'titre_page_configurer_flickr_rand' => 'Configurer Flickr hasard',

);
